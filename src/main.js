import Vue from 'vue'
import App from './App.vue'
import Gravatar from 'vue-gravatar'

Vue.config.productionTip = false

Vue.component('v-gravatar', Gravatar)

new Vue({
  render: h => h(App),
}).$mount('#app')
